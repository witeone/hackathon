CREATE TABLE patient
(
  firstName VARCHAR(128) NOT NULL,
  lastName VARCHAR(128) NOT NULL,
  sex VARCHAR(10) NOT NULL,
  BirthDate DATE NOT NULL,
  email VARCHAR(128) NOT NULL,
  pass VARCHAR(128) NOT NULL,
  pacientId INT NOT NULL,
  PRIMARY KEY (pacientId)
);

CREATE TABLE doctor
(
  DoctorId INT NOT NULL,
  name VARCHAR(128) NOT NULL,
  lastname VARCHAR(128) NOT NULL,
  pass VARCHAR(128) NOT NULL,
  PRIMARY KEY (DoctorId)
);

CREATE TABLE Medicament
(
  MedicamentID INT NOT NULL,
  MedName VARCHAR(120) NOT NULL,
  pcsNumber INT NOT NULL,
  PRIMARY KEY (MedicamentID)
);

CREATE TABLE Schedule
(
  Date DATE NOT NULL,
  isFromNet boolean NOT NULL,
  DoctorId INT NOT NULL,
  pacientId INT NOT NULL,
  PRIMARY KEY (Date, DoctorId, pacientId),
  FOREIGN KEY (DoctorId) REFERENCES doctor(DoctorId),
  FOREIGN KEY (pacientId) REFERENCES patient(pacientId)
);

CREATE TABLE Prescription
(
  prescriptionId INT NOT NULL,
  MedicamentID INT NOT NULL,
  PRIMARY KEY (prescriptionId),
  FOREIGN KEY (MedicamentID) REFERENCES Medicament(MedicamentID)
);

CREATE TABLE examination
(
  id INT NOT NULL,
  date DATE NOT NULL,
  patientID INT NOT NULL,
  doctorID INT NOT NULL,
  Diagnosis VARCHAR(120) NOT NULL,
  prescriptionId INT,
  PRIMARY KEY (id),
  FOREIGN KEY (patientID) REFERENCES patient(pacientId),
  FOREIGN KEY (doctorID) REFERENCES doctor(DoctorId),
  FOREIGN KEY (prescriptionId) REFERENCES Prescription(prescriptionId)
);