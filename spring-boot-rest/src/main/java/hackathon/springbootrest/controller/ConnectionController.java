package hackathon.springbootrest.controller;

import hackathon.springbootrest.entity.Examination;

import java.sql.*;

class ConnectionController {
	private static final String URL = "jdbc:postgresql://localhost:5432/postgres";
	private static final String USER = "postgres";
	private static final String PASSWORD = "postgres";

	private static final String SELECT_PATIENT_ID =
			"SELECT id FROM patient WHERE email = ?";

	private static final String SELECT_DOCTOR_ID =
			"SELECT id FROM doctor WHERE email = ?";

	private static final String SELECT_ID =
			"SELECT id FROM examination ORDER BY ID DESC LIMIT 1;";

	static long connectionTry(String query) throws DBException {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			try (PreparedStatement ps = connection.prepareStatement(query)) {
				ResultSet rs = ps.executeQuery();
				if (rs != null) {
					if (rs.next())
						return rs.getLong(1) + 1;
				}
			}
		} catch (SQLException e) {
			throw new DBException("Error getting id", e);
		}
		return 0;
	}

	static Examination mergeExaminations(String doctor, String patient) throws DBException {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Examination examination;
			PreparedStatement ps = connection.prepareStatement(SELECT_DOCTOR_ID);
			ps.setString(1, doctor);
			ResultSet rs = ps.executeQuery();
			if (rs != null) {
				if (rs.next()) {
					long docId = rs.getInt(1);
					examination = new Examination(connectionTry(SELECT_ID));
					examination.setDoctorId(docId);
				}
				else return null;
			}
			else return null;
			ps = connection.prepareStatement(SELECT_PATIENT_ID);
			ps.setString(1,patient);
			rs = ps.executeQuery();
			if (rs != null) {
				if (rs.next()) {
					long patId = rs.getInt(1);
					examination.setPatientId(patId);
					examination.setExaminationDate(new Timestamp(System.currentTimeMillis()));
					return examination;
				}
				else return null;
			}
			else return null;
		} catch (SQLException e) {
			throw new DBException("Error merging", e);
		}
	}
}
