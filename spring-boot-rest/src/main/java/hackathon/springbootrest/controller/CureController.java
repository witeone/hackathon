package hackathon.springbootrest.controller;

import hackathon.springbootrest.entity.Cure;
import hackathon.springbootrest.entity.request.AddCureRequest;
import hackathon.springbootrest.repository.CureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cures")
public class CureController {

	private static final String SELECT_ID =
			"SELECT id FROM cure ORDER BY ID DESC LIMIT 1;";

	private CureRepository cureRepository;

	@Autowired
	public CureController(CureRepository cureRepository) {
		this.cureRepository = cureRepository;
	}

	@RequestMapping(method = RequestMethod.GET)
	public List<Cure> findAllCures() {
		return cureRepository.findAll();
	}

	@RequestMapping(method = RequestMethod.POST)
	public void addCure(@RequestBody AddCureRequest addCureRequest) throws DBException {
		long id = ConnectionController.connectionTry(SELECT_ID);
		Cure cure = new Cure(id);
		cure.setMedName(addCureRequest.getMedName());
		cure.setPcsNumber(addCureRequest.getPcsNumber());
		cureRepository.save(cure);
	}
}
