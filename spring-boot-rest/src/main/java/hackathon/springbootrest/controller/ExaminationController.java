package hackathon.springbootrest.controller;

import hackathon.springbootrest.entity.Examination;
import hackathon.springbootrest.entity.request.AddExaminationRequest;
import hackathon.springbootrest.repository.ExaminationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/examinations")
public class ExaminationController {

    private static final String SELECT_ID =
            "SELECT id FROM examination ORDER BY ID DESC LIMIT 1;";

    private ExaminationRepository examinationRepository;

    @Autowired
    public ExaminationController(ExaminationRepository examineRepository) {
        this.examinationRepository = examineRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Examination> findAllExaminations() {
        return examinationRepository.findAll();
    }

    @GetMapping("/by-pacientid")
    public List<Examination> findPatientsExaminations(
            @RequestParam("patientId") int patientId){
        return examinationRepository.findByPatientId(patientId);
    }

    @GetMapping("/merge")
    public void findPatientsExaminations(
            @RequestParam("Pemail") String patientId,
            @RequestParam("Demail") String doctorId) throws DBException {
        examinationRepository.save(ConnectionController.mergeExaminations(doctorId, patientId));
    }


    @RequestMapping(method = RequestMethod.POST)
    public void addExamination(
            @RequestBody AddExaminationRequest addExaminationRequest) throws DBException {
        long id = ConnectionController.connectionTry(SELECT_ID);
        Examination examination = new Examination(id);
        examination.setDoctorId(addExaminationRequest.getDoctorId());
        examination.setExaminationDate(addExaminationRequest.getExaminationDate());
        examination.setPatientId(addExaminationRequest.getPatientId());
        examination.setPrescriptionId(addExaminationRequest.getPrescriptionId());
        examination.setDiagnosis(addExaminationRequest.getDiagnosis());

        examinationRepository.save(examination);
    }


}