package hackathon.springbootrest.controller;

import hackathon.springbootrest.entity.Patient;
import hackathon.springbootrest.entity.request.AddUserRequest;
import hackathon.springbootrest.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/patients")
public class PatientController {

	private static final String SELECT_ID =
			"SELECT id FROM patient ORDER BY ID DESC LIMIT 1;";

	private PatientRepository patientRepository;

	@Autowired
	public PatientController(PatientRepository patientRepository) {
		this.patientRepository = patientRepository;
	}

	@RequestMapping(method = RequestMethod.GET)
	public List<Patient> findAllUsers() {
		return patientRepository.findAll();
	}

	@GetMapping("/by-email")
	public Patient findPatientByEmail(@RequestParam("email") String email) {
		return patientRepository.findByEmail(email);
	}

	@GetMapping("/by-login")
	public Patient findPatientByEmail(@RequestParam("email") String email, @RequestParam("pass") String pass) {
		return patientRepository.findByEmailAndPass(email, pass);
	}

	@GetMapping("/by-reserve")
	public List<Patient> findFreeTime() {
		List<Patient> patientList = patientRepository.findAll();
		List<Patient> patients = new ArrayList<>();
		for (Patient patient: patientList) {
			if (patient.getReservation() != null) {
				patients.add(patient);
			}
		}
		return patients;
	}

	@RequestMapping(method = RequestMethod.POST)
	public void addUser(@RequestBody AddUserRequest addUserRequest) throws DBException {
		long id = ConnectionController.connectionTry(SELECT_ID);
		Patient patient = new Patient(id);
		patient.setName(addUserRequest.getName());
		patient.setLastname(addUserRequest.getLastname());
		patient.setDob(addUserRequest.getDob());
		patient.setSex(addUserRequest.getSex());
		patient.setEmail(addUserRequest.getEmail());
		patient.setPass(addUserRequest.getPass());
		patient.setDocMail(addUserRequest.getDocMail());
		patientRepository.save(patient);
	}
}
