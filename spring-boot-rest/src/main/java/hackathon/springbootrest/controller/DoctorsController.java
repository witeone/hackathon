package hackathon.springbootrest.controller;

import hackathon.springbootrest.entity.Doctor;
import hackathon.springbootrest.entity.request.AddDoctorRequest;
import hackathon.springbootrest.repository.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/doctors")
public class DoctorsController {

	private static final String SELECT_ID =
			"SELECT id FROM doctor ORDER BY ID DESC LIMIT 1;";

	private DoctorRepository doctorRepository;

	@Autowired
	public DoctorsController(DoctorRepository doctorRepository) {
		this.doctorRepository = doctorRepository;
	}

	@RequestMapping(method = RequestMethod.GET)
	public List<Doctor> findAllDoctors() {
		return doctorRepository.findAll();
	}

	@GetMapping("/by-email")
	public Doctor findDoctorByEmail(@RequestParam("email") String email) {
		return doctorRepository.findByEmail(email);
	}

	@RequestMapping(method = RequestMethod.POST)
	public void addUser(@RequestBody AddDoctorRequest addDoctorRequest) throws DBException {
		long id = ConnectionController.connectionTry(SELECT_ID);
		Doctor doctor = new Doctor(id);
		doctor.setName(addDoctorRequest.getName());
		doctor.setLastname(addDoctorRequest.getLastname());
		doctor.setEmail(addDoctorRequest.getEmail());
		doctor.setPass(addDoctorRequest.getPass());
		doctorRepository.save(doctor);
	}
}
