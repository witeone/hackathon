package hackathon.springbootrest.entity;

import javax.persistence.*;

@Entity
public class Cure {

	public Cure(long medicationID) {
		this.medicationID = medicationID;
	}

	private long medicationID;
	private String MedName;
	private int pcsNumber;

	public Cure() {
	}

	@Id
	public long getId() {
		return medicationID;
	}

	public void setId(long MedicamentID) {
		this.medicationID = MedicamentID;
	}

	public String getMedName() {
		return MedName;
	}

	public void setMedName(String medName) {
		MedName = medName;
	}

	public int getPcsNumber() {
		return pcsNumber;
	}

	public void setPcsNumber(int pcsNumber) {
		this.pcsNumber = pcsNumber;
	}
}
