package hackathon.springbootrest.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Patient {

	public Patient(long id) {
		Id = id;
	}

	private long Id;
	private String
			name,
			lastname,
			email,
			sex,
			pass,
			docMail;
	private Date dob, reservation;

	public Patient() {
	}

	@Id
	public long getId() {
		return Id;
	}

	public void setId(long Id) {
		this.Id = Id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getDocMail() {
		return docMail;
	}

	public void setDocMail(String docMail) {
		this.docMail = docMail;
	}

	public Date getReservation() {
		return reservation;
	}

	public void setReservation(Date reservation) {
		this.reservation = reservation;
	}
}
