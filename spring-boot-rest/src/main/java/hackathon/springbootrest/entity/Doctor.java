package hackathon.springbootrest.entity;

import javax.persistence.*;

@Entity
public class Doctor {

	public Doctor(long doctorId) {
		this.doctorId = doctorId;
	}

	private long doctorId;
	private String
			name,
			lastname,
			email,
			pass;

	public Doctor() {
	}

	@Id
	public long getId() {
		return doctorId;
	}

	public void setId(long doctorId) {
		this.doctorId = doctorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
}
