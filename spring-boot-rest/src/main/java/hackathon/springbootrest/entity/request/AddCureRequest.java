package hackathon.springbootrest.entity.request;

public class AddCureRequest {
	private String MedName;
	private int pcsNumber;

	public String getMedName() {
		return MedName;
	}

	public void setMedName(String medName) {
		MedName = medName;
	}

	public int getPcsNumber() {
		return pcsNumber;
	}

	public void setPcsNumber(int pcsNumber) {
		this.pcsNumber = pcsNumber;
	}
}
