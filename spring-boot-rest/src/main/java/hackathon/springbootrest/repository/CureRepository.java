package hackathon.springbootrest.repository;

import hackathon.springbootrest.entity.Cure;
import hackathon.springbootrest.entity.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CureRepository extends JpaRepository<Cure, Long> {
}
