import React, { Suspense } from 'react';
import { Route, Switch } from 'react-router';
import './App.css';
import Appbar from "./components/Appbar";
import Chat from './components/Chat';
import Profile from './components/Profile';
import Reservation from './components/Reservation';
import Doctors from './components/Doctors';

const Home = React.lazy(() => import("./components/Home"));

const App = props => {
  return (
    <div className='app'>
      <Suspense fallback={<div>Loading...</div>}>
        <Appbar />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/chat" component={Chat} />
          <Route exact path="/profile" component={Profile} />
          <Route exact path="/reservation" component={Reservation} />
          <Route exact path="/doctors" component={Doctors} />
        </Switch>
      </Suspense>
    </div>
  );
}

export default App;
