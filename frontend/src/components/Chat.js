import React, { useState } from 'react';
import './Chat.css';
import ChatMessage from './ChatMessage';

const msgs = [
    {to: "DoctorBot", msg: "Hello! Are you having difficulties, or do you need a prescription?", time: "31.3.2019", answers: ["Difficulties", "Prescription"], answersIndexes: [1, 8]},
    {to: "DoctorBot", msg: "Are you in pain?", time: "31.3.2019", answers: ["Yes", "No"], answersIndexes: [2, 2]},
    {to: "DoctorBot", msg: "Do you have cough?", time: "31.3.2019", answers: ["Yes", "No"], answersIndexes: [3, 3]},
    {to: "DoctorBot", msg: "Have you had a fever?", time: "31.3.2019", answers: ["Yes", "No"], answersIndexes: [4, 4]},
    {to: "DoctorBot", msg: "Have you vomited?", time: "31.3.2019", answers: ["Yes", "No"], answersIndexes: [5, 5]},
    {to: "DoctorBot", msg: "Have you had diarrhea?", time: "31.3.2019", answers: ["Yes", "No"], answersIndexes: [6, 6]},
    {to: "DoctorBot", msg: "Is it needed to attend personally?", time: "31.3.2019", answers: ["Yes", "No"], answersIndexes: [7, 7]},
    {},
    {to: "DoctorBot", msg: "Is it needed to attend personally?", time: "31.3.2019", answers: ["Yes", "No"], answersIndexes: [7, 7]},
];

const Chat = props => {
    const [msgsToRender, setMsgsToRender] = useState([msgs[0]]);
    return (
        <div className="chat-wrapper">
            <div className="chat-msg-section">
                {msgsToRender.map(msg => <ChatMessage msgs={msgs} msg={msg} decision={idx => setMsgsToRender([...msgsToRender, msgs[idx]])}/>)}
            </div>
        </div>
    )
}

export default Chat;