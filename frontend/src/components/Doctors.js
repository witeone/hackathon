import React, {useEffect, useState} from 'react';
import './Doctors.css';

const Doctors = props => {
    const [doctors, setDoctors] = useState(null);
    useEffect(() => {
        fetch("http://10.236.254.148:8080/doctors")
        .then(data => data.json())
        .then(data => setDoctors(data))
    }, [])
    return (
        <div className="doctors-wrapper">
            {
                doctors && doctors.map((doctor, index) => {
                    return (
                        <section key={index}>
                            <div className="doctors-heading">
                                <h2>{doctor.name} {doctor.lastname}</h2>
                            </div>                            
                            <hr />
                            <div className="doctors-info">
                                <p>Firstname</p>
                                <p>{doctor.name}</p>
                            </div>
                            <div className="doctors-info">
                                <p>Lastname</p>
                                <p>{doctor.lastname}</p>
                            </div>
                            <div className="doctors-info">
                                <p>E-mail</p>
                                <p>{doctor.email}</p>
                            </div>
                        </section>
                    )
                })
            }
        </div>
    )
}

export default Doctors; 