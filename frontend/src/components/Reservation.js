import React, { useEffect, useState } from 'react';
import "./Reservation.css";

const freeTimes = [
    {
        day: "15.04.2019",
        times: [
            "08:00", "08:20", "08:40", "09:00", "09:20", "09:40", "10:00", "10:20", "10:40", "11:00", "11:20", "11:40", "12:00", "12:20", "12:40", "13:00", "13:20", "13:40"
        ]
    },
    {
        day: "16.04.2019",
        times: [
            "08:00", "08:20", "08:40", "09:00", "09:20", "09:40", "10:00", "10:20", "10:40", "11:00", "11:20", "11:40", "12:00", "12:20", "12:40", "13:00", "13:20", "13:40"
        ]
    },
    {
        day: "17.04.2019",
        times: [
            "08:00", "08:20", "08:40", "09:00", "09:20", "09:40", "10:00", "10:20", "10:40", "11:00", "11:20", "11:40", "12:00", "12:20", "12:40", "13:00", "13:20", "13:40"
        ]
    },
    {
        day: "18.04.2019",
        times: [
            "08:00", "08:20", "08:40", "09:00", "09:20", "09:40", "10:00", "10:20", "10:40", "11:00", "11:20", "11:40", "12:00", "12:20", "12:40", "13:00", "13:20", "13:40"
        ]
    },
    {
        day: "19.04.2019",
        times: [
            "08:00", "08:20", "08:40", "09:00", "09:20", "09:40", "10:00", "10:20", "10:40", "11:00", "11:20", "11:40", "12:00", "12:20", "12:40", "13:00", "13:20", "13:40"
        ]
    }
]

const Reservation = props => {
    const [reservedTimes, setReservedTimes] = useState([]);

    useEffect(() => {
        fetch("http://10.236.254.148:8080/patients/by-reserve")
        .then(data => data.json())
        .then(data => data.map(person => {
            var hour = (parseInt(person.reservation.slice(11, 13)) + 2) % 24;
            const newTime = `${hour < 10 ? '0' + hour : hour}:${person.reservation.slice(14, 16)}`;
            console.log(person.reservation);
            setReservedTimes([...reservedTimes, newTime]);
            return null;
        }))
    },[])

    return (
        <div className="reservation-wrapper">
            <div className="reservation-time">
                <h2>Next patient number: [15]</h2>
                <div className="reservation-clock">
                    1 hour 6 minutes
                </div>
                <div>
                    at 11:20
                </div>
            </div>
            <div className="reservation-table">
                {
                    freeTimes.map((day, index) => {
                        return (
                            <div key={index} className="reservation-day">
                                <p>{day.day}</p>
                                {
                                    day.times.map((time, index) => <div key={index} className={reservedTimes !== [] && (reservedTimes.filter(x => x === time).length > 0 && day.day === "15.04.2019" ? "reservation-day-time reserved" : "reservation-day-time")}>{time}</div>)
                                }
                            </div>
                        );
                    })
                }
            </div>
        </div>
    )
}

export default Reservation;