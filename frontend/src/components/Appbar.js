import React from 'react';
import { NavLink } from 'react-router-dom';
import "./Appbar.css";

const Appbar = props => {
  return (
    <div className="appbar-wrapper">
      <div className="uselessDiv" />
      <div className="appbar-left">
        <NavLink activeClassName="appbar-active" to='/chat'>doctor bot</NavLink>
        <NavLink activeClassName="appbar-active" to='/reservation'>Reservation</NavLink>
        <NavLink activeClassName="appbar-active" to='/profile'>Profile</NavLink>
        <NavLink activeClassName="appbar-active" to='/doctors'>Doctors</NavLink>
      </div>
      <div className="appbar-right">
        {/* <NavLink activeClassName="appbar-active" to='/login'>Login</NavLink> */}
      </div>
    </div>
  );
}

export default Appbar;