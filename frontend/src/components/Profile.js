import React, {useState, useEffect} from 'react';
import './Profile.css';

const Profile = props => {
    const [user, setUser] = useState(null);
    const [doctor, setDoctor] = useState(null);
    const [examinations, setExaminations] = useState(null);

    useEffect(() => {
        fetch("http://10.236.254.148:8080/patients/by-email?email=zmina.ta@d-link.ml")
        .then(data => data.json())
        .then(data => {
            setUser(data);
            fetch(`http://10.236.254.148:8080/doctors/by-email?email=${data.docMail}`)
            .then(data => data.json())
            .then(data => setDoctor(data))
            fetch(`http://10.236.254.148:8080/examinations/by-pacientid?patientId=${data.id}`)
            .then(data => data.json())
            .then(data => setExaminations(data))
        })
    }, []);

    return (
        <div className="profile-wrapper">
            <section>
                <div className="profile-section-header">
                    <h2>User information</h2>
                    <i className="material-icons">create</i>
                </div>
                <hr />
                <div className="profile-user-info">
                    <p>Firstname</p>
                    <p>{user ? user.name : ""}</p>
                </div>
                <div className="profile-user-info">
                    <p>Lastname</p>
                    <p>{user ? user.lastname : ""}</p>
                </div>
                <div className="profile-user-info">
                    <p>E-mail</p>
                    <p>{user ? user.email : ""}</p>
                </div>
                <div className="profile-user-info">
                    <p>Date of birth</p>
                    <p>{user ? user.dob.slice(0,10) : ""}</p>
                </div>
                <div className="profile-user-info">
                    <p>Sex</p>
                    <p>{user ? (user.sex === 'male' ? "M" : "F") : ""}</p>
                </div>
            </section>

            <section>
                <div className="profile-section-header">
                    <h2>Health information</h2>
                    <i className="material-icons">create</i>
                </div>
                <hr />
                <div className="profile-health-examinations">
                    <p>Examinations</p>
                    <div className="examinations">
                        {
                            examinations && examinations.map((examination, index) => <p key={index}><b>{examination.diagnosis}</b> ({examination.examinationDate.slice(0, 10)})</p>)
                        }
                    </div>
                </div>
                <div className="profile-health-receipt">
                    <p>Receipts</p>
                    <div className="receipts">
                        <p>Paralen</p>
                        <p>Ibuprofen</p>
                    </div>
                </div>
            </section>

            <section>
                <div className="profile-section-header">
                    <h2>Doctor information</h2>
                    <i className="material-icons">create</i>
                </div>
                <hr />
                <div className="profile-user-info">
                    <p>Firstname</p>
                    <p>{doctor ? doctor.name : ""}</p>
                </div>
                <div className="profile-user-info">
                    <p>Lastname</p>
                    <p>{doctor ? doctor.lastname : ""}</p>
                </div>
                <div className="profile-user-info">
                    <p>E-mail</p>
                    <p>{doctor ? doctor.email : ""}</p>
                </div>
                <div className="profile-user-info">
                    <p>Sex</p>
                    <p>{doctor ? (doctor.sex === 'male' ? "M" : "F") : ""}</p>
                </div>
            </section>
        </div>
    )
}

export default Profile;