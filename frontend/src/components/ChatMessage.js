import React, { useState } from 'react';
import './ChatMessage.css';

const ChatMessage = props => {
    return (
        <div className="chatmessage-wrapper">
            <div className="chatmessage-info">
                <img className="avatar" alt="doctor" src="https://image.flaticon.com/icons/svg/387/387561.svg"/>
                <div className="chatmessage-msg">
                    <p id="recipient">{props.msg.to}</p>
                    <p id="time">{props.msg.time}</p>
                    <p id="message">{props.msg.msg}</p>
                </div>
            </div>
            <div className="btn-group">
                {
                    props.msg.answers.map((answer, idx) => {
                        const [isClicked, setIsClicked] = useState(false);
                        return <button style={{ background: isClicked ? "#F39A24" : "white", color: isClicked ? "white" : "#F39A24" }} onClick={() => { props.decision(props.msg.answersIndexes[idx]);setIsClicked(true) }}>{answer}</button>
                    })
                }
            </div>
        </div>
    )
}

export default ChatMessage;