import React from 'react';
import "./Home.css";
import logo from "../img/home-background.svg";

const Home = props => {
    return (
        <div className="home-wrapper">
            <img src={logo} alt="Home logo" />
            <section>
                <h1>Hippocrates</h1>
                <h2>mediworx challange</h2>
                <p>Aiming for increased efficiency of regular check at the doctor using basic info provided by the patient,
                     selecting an appointment from avaliable time units, and to minimize and ideally to eliminate waiting times.</p>
            </section>
        </div>
    );
}

export default Home;